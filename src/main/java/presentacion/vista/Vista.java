package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;

import persistencia.conexion.Conexion;

public class Vista
{
	private JFrame frame;
	private JMenuBar barra;
	private JMenu menu;
	private JMenuItem menuItem;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private JButton btnABMTipoContacto;
	private JButton btnLocalidad;
	private JScrollPane spPersonas;
	private JTable tablaPersonas;
	private DefaultTableModel modelPersonas;
	private String[] nombreColumnas = {"id","Nombre y apellido","Telefono","Calle","Altura","Piso","Departamento","Localidad","Email","Fecha de nacimiento","Tipo"};

	public Vista() {
		super();
		initialize();
	}


	private void initialize() {
		frame = new JFrame();
		frame.setBounds(120, 120, 660, 280);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Agenda");
		
		JPanel panel           = new JPanel();
		barra                  = new JMenuBar();
		menu                   = new JMenu("Configuración");
		menuItem               = new JMenuItem("Conexión");
		
		menu .add(menuItem);
		barra.add(menu);
		frame.setJMenuBar(barra);
		
		panel.setBounds(0, 0, 650, 262);
		panel.setLayout(null);
		frame.getContentPane().add(panel);
		
		spPersonas         = new JScrollPane();
		modelPersonas      = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas      = new JTable(modelPersonas);
		btnAgregar         = new JButton("Agregar");
		btnEditar          = new JButton("Editar");
		btnABMTipoContacto = new JButton("Tipos de contacto");
		btnBorrar          = new JButton("Borrar");
		btnReporte         = new JButton("Reporte");
		btnLocalidad       = new JButton("Localidades");

		spPersonas         .setViewportView(tablaPersonas);
		btnAgregar         .setToolTipText("Agregar");
		btnEditar          .setToolTipText("Editar");
		btnABMTipoContacto .setToolTipText("ABM tipo contacto");
		btnBorrar          .setToolTipText("Borrar");
		btnReporte         .setToolTipText("Reporte");
		btnLocalidad       .setToolTipText("ABM localidad");
		
		spPersonas         .setBounds(10, 11, 630, 182);
		btnAgregar         .setBounds(5  , 200, 100, 23);
		btnEditar          .setBounds(110, 200, 100, 23);
		btnABMTipoContacto .setBounds(215, 200, 100, 23);
		btnBorrar          .setBounds(320, 200, 100, 23);
		btnReporte         .setBounds(425, 200, 100, 23);
		btnLocalidad       .setBounds(530, 200, 100, 23);

		panel.add(spPersonas);
		panel.add(btnAgregar);		
		panel.add(btnEditar);		
		panel.add(btnABMTipoContacto);		
		panel.add(btnBorrar);
		panel.add(btnReporte);
		panel.add(btnLocalidad);
	}
	
	public void show()
	{
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "Estas seguro que quieres salir de la Agenda!?", 
		             "Confirmacion", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frame.setVisible(true);
	}
	
	public JFrame getFrame() {
		return frame;
	}
	
	public JMenuBar getBarra() {
		return barra;
	}
	
	public JMenu getMenu() {
		return menu;
	}
	
	public JMenuItem getMenuItem() {
		return menuItem;
	}
	
	public JButton getBtnAgregar() {
		return btnAgregar;
	}
	
	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}
	
	public JButton getBtnReporte() {
		return btnReporte;
	}
	
	public JButton getBtnABMTipoContacto() {
		return btnABMTipoContacto;
	}
	
	public JButton getBtnABMLocalidad() {
		return btnLocalidad;
	}
	
	public DefaultTableModel getModelPersonas() {
		return modelPersonas;
	}
	
	public JTable getTablaPersonas() {
		return tablaPersonas;
	}
	
	public JScrollPane getSpPersonas() {
		return spPersonas;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}
}
