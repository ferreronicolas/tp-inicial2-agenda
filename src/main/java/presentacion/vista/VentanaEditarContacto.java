package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.github.lgooddatepicker.components.DatePicker;

import dto.LocalidadDTO;
import dto.TipoContactoDTO;

public class VentanaEditarContacto extends JDialog {

	private static final long serialVersionUID = 1L;
	private JLabel lblNombre;
	private JLabel lblTelefono;
	private JLabel lblCalle;
	private JLabel lblAltura;
	private JLabel lblPiso;
	private JLabel lblDepto;
	private JLabel lblLocalidad;
	private JLabel lblEmail;
	private JLabel lblCumple;
	private JLabel lblTipoContacto;
	
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JTextField txtPiso;
	private JTextField txtDepto;
	private JComboBox<LocalidadDTO> comboLocalidad;
	private JTextField txtEmail;
	private DatePicker datePicker;
	private JComboBox<TipoContactoDTO> comboTipoContacto;
	
	private int idContacto;
	
	private JButton btnEditar;
	private JButton btnSalir;
	
	private static VentanaEditarContacto instancia;
	
	public static VentanaEditarContacto getInstance(JFrame frame) {
		if( instancia == null )
			return new VentanaEditarContacto(frame);
		else
			return instancia;
	}
	
	private VentanaEditarContacto(JFrame frame) {
		super(frame,true);
		setBounds(200, 200, 410, 380);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle("Editar contacto");
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 10, this.getWidth() - 10, this.getHeight() - 10);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		this.idContacto = 0;
		
		lblNombre            = new JLabel("Nombre: ");
		lblTelefono          = new JLabel("Teléfono: ");
		lblCalle             = new JLabel("Calle: ");
		lblAltura            = new JLabel("Altura: ");
		lblPiso              = new JLabel("Piso: ");
		lblDepto             = new JLabel("Departamento: ");
		lblLocalidad         = new JLabel("Localidad: ");
		lblEmail             = new JLabel("Email: ");
		lblCumple            = new JLabel("Fecha de nacimiento: ");
		lblTipoContacto      = new JLabel("Tipo de contacto: ");
		
		lblNombre           .setBounds(10, 10, 160, 25);
		lblTelefono         .setBounds(10, 40, 160, 25);
		lblCalle            .setBounds(10, 70, 160, 25);
		lblAltura           .setBounds(10, 100, 160, 25);
		lblPiso             .setBounds(10, 130, 160, 25);
		lblDepto            .setBounds(10, 160, 160, 25);
		lblLocalidad        .setBounds(10, 190, 160, 25);
		lblEmail            .setBounds(10, 220, 160, 25);
		lblCumple           .setBounds(10, 250, 160, 25);		
		lblTipoContacto     .setBounds(10, 280, 160, 25);		
		
		panel.add(lblNombre);
		panel.add(lblTelefono);
		panel.add(lblCalle);
		panel.add(lblAltura);
		panel.add(lblPiso);
		panel.add(lblDepto);
		panel.add(lblLocalidad);
		panel.add(lblEmail);
		panel.add(lblCumple);
		panel.add(lblTipoContacto);
		
		txtNombre          = new JTextField();
		txtTelefono        = new JTextField();
		txtCalle           = new JTextField();
		txtAltura          = new JTextField();
		txtPiso            = new JTextField();
		txtDepto           = new JTextField();
		comboLocalidad     = new JComboBox<LocalidadDTO>();
		txtEmail           = new JTextField();
		datePicker         = new DatePicker();
		comboTipoContacto  = new JComboBox<TipoContactoDTO>();
		
		txtNombre         .setBounds(170, 10,  220, 25);
		txtTelefono       .setBounds(170, 40,  220, 25);
		txtCalle          .setBounds(170, 70,  220, 25);
		txtAltura         .setBounds(170, 100, 220, 25);
		txtPiso           .setBounds(170, 130, 220, 25);
		txtDepto          .setBounds(170, 160, 220, 25);
		comboLocalidad    .setBounds(170, 190, 220, 25);
		txtEmail          .setBounds(170, 220, 220, 25);
		datePicker        .setBounds(170, 250, 220, 25);
		comboTipoContacto .setBounds(170, 280, 220, 25);
		
		panel.add(txtNombre);
		panel.add(txtTelefono);
		panel.add(txtCalle);
		panel.add(txtAltura);
		panel.add(txtPiso);
		panel.add(txtDepto);
		panel.add(comboLocalidad);
		panel.add(txtEmail);
		panel.add(datePicker);
		panel.add(comboTipoContacto);
		
		btnEditar = new JButton("Editar");	
		btnSalir  = new JButton("Salir");
		
		btnEditar.setBounds(30, 310, 100, 25);
		btnSalir .setBounds(210, 310, 100, 25);
		
		panel.add(btnEditar);
		panel.add(btnSalir);
		
		setVisible(false);
	}
	
	public void mostrarVentana() {
		this.setVisible(true);
	}
	
	public void cerrar() {
		idContacto = 0;
		txtNombre         .setText(null);
		txtTelefono       .setText(null);
		txtCalle          .setText(null);
		txtAltura         .setText(null);
		txtPiso           .setText(null);
		txtDepto          .setText(null);
		txtEmail          .setText(null);
		dispose();
	}

	public JLabel getLblNombre() {
		return lblNombre;
	}

	public JLabel getLblTelefono() {
		return lblTelefono;
	}

	public JLabel getLblCalle() {
		return lblCalle;
	}

	public JLabel getLblAltura() {
		return lblAltura;
	}

	public JLabel getLblPiso() {
		return lblPiso;
	}

	public JLabel getLblDepto() {
		return lblDepto;
	}

	public JLabel getLblLocalidad() {
		return lblLocalidad;
	}

	public JLabel getLblEmail() {
		return lblEmail;
	}

	public JLabel getLblCumple() {
		return lblCumple;
	}

	public JLabel getLblTipoContacto() {
		return lblTipoContacto;
	}

	public int getIdContacto() {
		return idContacto;
	}

	public void setIdContacto(int idContacto) {
		this.idContacto = idContacto;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	public JTextField getTxtCalle() {
		return txtCalle;
	}

	public JTextField getTxtAltura() {
		return txtAltura;
	}

	public JTextField getTxtPiso() {
		return txtPiso;
	}

	public JTextField getTxtDepto() {
		return txtDepto;
	}

	public JComboBox<LocalidadDTO> getComboLocalidad() {
		return comboLocalidad;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public DatePicker getDatePiker() {
		return datePicker;
	}
	
	public JComboBox<TipoContactoDTO> getComboTipoContacto() {
		return comboTipoContacto;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JButton getBtnSalir() {
		return btnSalir;
	}
}
