package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class VentanaLocalidad extends JDialog {

	private static final long serialVersionUID = 1L;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JTable tablaLocalidades;
	private DefaultTableModel modelLocalidades;
	private String[] nombreColumnas = {"nombre"};
	private static VentanaLocalidad instancia;
	
	public static VentanaLocalidad getInstance(JFrame parent) {
		if(instancia == null)
			return new VentanaLocalidad(parent);
		else
			return instancia;
	}

	private VentanaLocalidad(JFrame parent) {
		super(parent, true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(150, 150, 350, 300);
		setTitle("Localidad");
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 10, this.getWidth() - 10, this.getHeight() - 10);
		this.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spLocalidades = new JScrollPane();
		spLocalidades.setBounds(10, 20, panel.getWidth() - 20, panel.getHeight() / 2);
		panel.add(spLocalidades);
		
		modelLocalidades = new DefaultTableModel(null, nombreColumnas);
		tablaLocalidades = new JTable(modelLocalidades);
		
		spLocalidades.setViewportView(tablaLocalidades);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(panel.getX() + 10, spLocalidades.getHeight() + 50, 89, 23);
		btnAgregar.setToolTipText("Agregar");
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(btnAgregar.getX() + btnAgregar.getWidth() + 20, spLocalidades.getHeight() + 50, 89, 23);
		btnEditar.setToolTipText("Editar");
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(btnEditar.getX() + btnEditar.getWidth() + 20, spLocalidades.getHeight() + 50, 89, 23);
		btnBorrar.setToolTipText("Borrar");
		panel.add(btnBorrar);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana() {
		this.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public JTable getTablaLocalidades() {
		return tablaLocalidades;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public DefaultTableModel getModelLocalidades() {
		return modelLocalidades;
	}
}
