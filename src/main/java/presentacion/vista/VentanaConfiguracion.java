package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class VentanaConfiguracion extends JDialog {

	private static final long serialVersionUID = 1L;
	private JLabel lblPuerto;
	private JLabel lblNombre;
	private JLabel lblUsuario;
	private JLabel lblContrasena;
	private JTextField txtPuerto;
	private JTextField txtNombre;
	private JTextField txtUsuario;
	private JPasswordField txtContrasena;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private static VentanaConfiguracion instancia;
	
	public static VentanaConfiguracion getInstance(JFrame parent) {
		if( instancia == null )
			return new VentanaConfiguracion(parent);
		else 
			if( instancia.getParent().equals(parent) )
				return instancia;
			else
				throw new IllegalArgumentException("La ventana ya fue creada con un parent distinto.");
	}
	
	private VentanaConfiguracion(JFrame parent) {
		super(parent, true);
		setDefaultCloseOperation(0);
		setDefaultLookAndFeelDecorated(true);
		setBounds(200, 200, 370, 200);
		setTitle("Configuración de la conexión");
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 10, this.getWidth() - 10, this.getHeight() - 10);
		panel.setLayout(null);
		getContentPane().add(panel);
		
		lblPuerto     = new JLabel("Puerto: ");
		lblNombre     = new JLabel("Nombre de la BD: ");
		lblUsuario    = new JLabel("Usuario: ");
		lblContrasena = new JLabel("Contraseña: ");
		
		lblPuerto     .setBounds(10, 10,  160, 25);
		lblNombre     .setBounds(10, 40,  160, 25);
		lblUsuario    .setBounds(10, 70,  160, 25);
		lblContrasena .setBounds(10, 100, 160, 25);
		
		panel.add(lblPuerto);
		panel.add(lblNombre);
		panel.add(lblUsuario);
		panel.add(lblContrasena);
		
		txtPuerto     = new JTextField();
		txtNombre     = new JTextField();
		txtUsuario    = new JTextField();
		txtContrasena = new JPasswordField();
		
		txtPuerto     .setBounds(170, 10,  180, 25);
		txtNombre     .setBounds(170, 40,  180, 25);
		txtUsuario    .setBounds(170, 70,  180, 25);
		txtContrasena .setBounds(170, 100, 180, 25);
		
		panel.add(txtPuerto);
		panel.add(txtNombre);
		panel.add(txtUsuario);
		panel.add(txtContrasena);
		
		btnAceptar  = new JButton("Aceptar");
		btnCancelar = new JButton("Cancelar");
		
		btnAceptar.setBounds(10, 130, 120, 25);
		btnCancelar.setBounds(170, 130, 120, 25);
		
		panel.add(btnAceptar);
		panel.add(btnCancelar);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana() {
		this.setVisible(true);
	}
	
	public void cerrar() {
		this.txtPuerto.setText(null);
		this.txtNombre.setText(null);
		this.txtUsuario.setText(null);
		this.txtContrasena.setText(null);
		this.dispose();
	}

	public JLabel getLblPuerto() {
		return lblPuerto;
	}

	public JLabel getLblNombre() {
		return lblNombre;
	}

	public JLabel getLblUsuario() {
		return lblUsuario;
	}

	public JLabel getLblContrasena() {
		return lblContrasena;
	}

	public JTextField getTxtPuerto() {
		return txtPuerto;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public JPasswordField getTxtContrasena() {
		return txtContrasena;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}
}
