package presentacion.controlador;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import dto.ConfiguracionDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;
import modelo.Agenda;
import modelo.SettingsManager;
import modelo.Validador;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaConfiguracion;
import presentacion.vista.VentanaEditarContacto;
import presentacion.vista.VentanaLocalidad;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VentanaTipoContacto;
import presentacion.vista.Vista;

public class Controlador {
	
		private Vista vista;
		private List<PersonaDTO> personas_en_tabla;
		private VentanaPersona ventanaPersona;
		private VentanaEditarContacto ventanaEditarContacto;
		private VentanaLocalidad ventanaLocalidad;
		private VentanaTipoContacto ventanaTipoContacto;
		private VentanaConfiguracion ventanaConfiguracion;
		private Agenda agenda;
		
		private Logger log = Logger.getLogger(getClass());
		
		private ConfiguracionDTO configuracion = null;
		private SettingsManager settingsManager;
		
		public Controlador (Vista vista) {
			settingsManager = new SettingsManager();
			this.vista = vista;
			BufferedImage img = null;
			InputStream file = getClass().getClassLoader().getResourceAsStream("agendaIcon.png");		
			try {
				img = ImageIO.read(file);
				this.vista.getFrame().setIconImage(img);
			} catch (Exception e) {
				log.error("No se pudo cargar el icono: " + e.getMessage());
			}
			this.ventanaConfiguracion = VentanaConfiguracion.getInstance( this.vista.getFrame() );
			this.ventanaConfiguracion.getBtnAceptar().addActionListener( a -> guardarValores() );
			this.ventanaConfiguracion.getBtnCancelar().addActionListener( a -> ventanaConfiguracion.cerrar() );
		}
		
		public Controlador(Vista vista, Agenda agenda) {
			this.vista = vista;
			this.vista.getBtnAgregar().addActionListener( a -> ventanaAgregarPersona() );
			this.vista.getBtnBorrar().addActionListener( s -> borrarPersona() );
			this.vista.getBtnEditar().addActionListener( a -> ventanaEditarContacto() );
			this.vista.getBtnReporte().addActionListener( r -> mostrarReporte() );
			this.vista.getBtnABMLocalidad().addActionListener( l -> ventanaLocalidad() );
			this.vista.getBtnABMTipoContacto().addActionListener( t -> ventanaTipoContacto() );
			this.vista.getMenuItem().addActionListener( a -> ventanaConfiguracion() );
			
			this.ventanaPersona = VentanaPersona.getInstance( this.vista.getFrame() );
			this.ventanaPersona.getBtnAgregarPersona().addActionListener( p -> guardarPersona() );
			this.ventanaPersona.getDatePicker().getSettings().setLocale( agenda.getLocale() );
			
			this.ventanaEditarContacto = VentanaEditarContacto.getInstance(this.vista.getFrame());
			this.ventanaEditarContacto.getBtnEditar().addActionListener( a -> editarContacto() );
			this.ventanaEditarContacto.getBtnSalir().addActionListener( a -> this.ventanaEditarContacto.cerrar() );
			this.ventanaEditarContacto.getDatePiker().getSettings().setLocale( agenda.getLocale() );
			
			this.ventanaLocalidad = VentanaLocalidad.getInstance( this.vista.getFrame() );
			this.ventanaLocalidad.getBtnAgregar().addActionListener( j -> agregarLocalidad() );
			this.ventanaLocalidad.getBtnEditar().addActionListener( a -> editarLocalidad() );
			this.ventanaLocalidad.getBtnBorrar().addActionListener( a -> borrarLocalidad() );
			
			this.ventanaTipoContacto = VentanaTipoContacto.getInstance( this.vista.getFrame() );
			this.ventanaTipoContacto.getBtnAgregar().addActionListener( a -> agregarTipoContacto() );
			this.ventanaTipoContacto.getBtnEditar().addActionListener( a -> editarTipoContacto() );
			this.ventanaTipoContacto.getBtnBorrar().addActionListener( a -> borrarTipoContacto());
			
			this.ventanaConfiguracion = VentanaConfiguracion.getInstance( this.vista.getFrame() );
			this.ventanaConfiguracion.getBtnAceptar().addActionListener( a -> guardarValores() );
			this.ventanaConfiguracion.getBtnCancelar().addActionListener( a -> restablecerValores() );

			this.agenda = agenda;
			settingsManager = new SettingsManager();
			
			this.personas_en_tabla = null;
		}

		// ** Inicializador **
		public void inicializar() {
			this.llenarTabla();
			this.llenarTablaLocalidades();
			this.llenarTablaTipoContacto();
			this.llenarCombos();
			this.vista.show();
		}
		
		
		// ** Configuración **
		private void restablecerValores() {
			this.configuracion = this.settingsManager.getSettings();
			Validador.validarConexion(this.configuracion);
			this.ventanaConfiguracion.cerrar();
		}
		
		public void getConfiguracion() {
			this.configuracion = null;
			this.ventanaConfiguracion.mostrarVentana();
		}
		
		private void guardarValores() {
			this.configuracion = new ConfiguracionDTO(this.ventanaConfiguracion.getTxtPuerto().getText(), this.ventanaConfiguracion.getTxtNombre().getText(), this.ventanaConfiguracion.getTxtUsuario().getText(), new String(this.ventanaConfiguracion.getTxtContrasena().getPassword()));
			if( this.configuracion == null || !Validador.validarConexion(this.configuracion)) {
				JOptionPane.showMessageDialog(null, "No se pudo realizar la conexión.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
				this.getConfiguracion();
			}else {
				this.settingsManager.setSettings(this.configuracion);
				this.ventanaConfiguracion.cerrar();
			}
			
		}
		
		public ConfiguracionDTO configurarConexion() {
			this.configuracion = settingsManager.getSettings();
			if( this.configuracion == null )
				this.getConfiguracion();
			if( this.configuracion != null ) {
				this.settingsManager.setSettings(this.configuracion);
				return this.configuracion;
			}
			return null;
		}
		
		private void ventanaConfiguracion() {
			this.configuracion = settingsManager.getSettings();
			this.ventanaConfiguracion.getTxtPuerto().setText(this.configuracion.getPuerto());
			this.ventanaConfiguracion.getTxtNombre().setText(this.configuracion.getNombre());
			this.ventanaConfiguracion.getTxtUsuario().setText(this.configuracion.getUsuario());
			this.ventanaConfiguracion.mostrarVentana();
		}
		
		
		
		
		// ** Localidad **
		private void ventanaLocalidad() {
			this.ventanaLocalidad.mostrarVentana();
		}
		
		private void agregarLocalidad() {
			String localidadNueva = JOptionPane.showInputDialog(ventanaLocalidad, "Nombre para la localidad: ", "Agregar localidad", JOptionPane.QUESTION_MESSAGE);
			if( localidadNueva != null )
				if( localidadNueva.trim().isEmpty() )
					JOptionPane.showMessageDialog(ventanaLocalidad, "El nombre de la localidad no puede estar vacío.", "No se pudo agregar la localidad", JOptionPane.ERROR_MESSAGE);
				else {
					LocalidadDTO nuevaLocalidad = new LocalidadDTO(0, localidadNueva);
					this.agenda.agregarLocalidad(nuevaLocalidad);
					this.llenarTablaLocalidades();
					this.llenarCombos();
				}
		}
		
		private void editarLocalidad() {
			JTable tablaLocalidades = this.ventanaLocalidad.getTablaLocalidades();
			int filaSeleccionada = tablaLocalidades.getSelectedRow();
			if( filaSeleccionada == -1 ) {
				JOptionPane.showMessageDialog(this.ventanaLocalidad, "No hay ninguna localidad seleccionada.", "Error", JOptionPane.WARNING_MESSAGE);
			} else {
				LocalidadDTO localidadAEditar = (LocalidadDTO) tablaLocalidades.getValueAt(filaSeleccionada, 0);
				String nuevoNombre = JOptionPane.showInputDialog(ventanaLocalidad, "Ingrese un nuevo nombre para la localidad: ", localidadAEditar.getNombre());
				if( nuevoNombre != null )
					if( nuevoNombre.trim().isEmpty() )
						JOptionPane.showMessageDialog(ventanaLocalidad, "El nuevo nombre de la localidad no puede estar vacío.", "No se pudo agregar la localidad", JOptionPane.ERROR_MESSAGE);
					else {
						localidadAEditar.setNombre(nuevoNombre);
						this.agenda.actualizarLocalidad(localidadAEditar);
						this.llenarTablaLocalidades();
						this.llenarCombos();
						this.llenarTabla();
					}
			}
		}
		
		public void borrarLocalidad() {
			int[] filasSeleccionadas = this.ventanaLocalidad.getTablaLocalidades().getSelectedRows();
			if( filasSeleccionadas.length == 0 )
				JOptionPane.showMessageDialog(this.ventanaLocalidad, "No hay ninguna localidad seleccionada.", "Error", JOptionPane.WARNING_MESSAGE);
			else {
				LocalidadDTO localidadABorrar;
				for( int filaSeleccionada : filasSeleccionadas ) {
					localidadABorrar = (LocalidadDTO) this.ventanaLocalidad.getTablaLocalidades().getValueAt(filaSeleccionada, 0);
					if( !this.agenda.borrarLocalidad(localidadABorrar) )
						JOptionPane.showMessageDialog(ventanaLocalidad, "La localidad " + localidadABorrar.getNombre() + " no se puede borrar porque está relacionada con uno o más contactos.\n Edite los contactos que vivan en esta localidad antes de borrarla.", "No se pudo borrar la localidad", JOptionPane.ERROR_MESSAGE);
					}
				this.llenarTablaLocalidades();
				this.llenarCombos();
			}
		}
				
		
		// ** Tipo contacto **
		private void ventanaTipoContacto() {
			this.ventanaTipoContacto.mostrarVentana();
		}
		
		private void agregarTipoContacto() {			
			String tipoContactoNuevo = JOptionPane.showInputDialog(ventanaTipoContacto, "Nombre para el tipo de contacto: ", "Agregar tipo de contacto", JOptionPane.QUESTION_MESSAGE);
			if( tipoContactoNuevo != null )
				if( tipoContactoNuevo.trim().isEmpty() )
					JOptionPane.showMessageDialog(ventanaLocalidad, "El nombre del tipo de contacto no puede estar vacío.", "No se pudo agregar el tipo de contacto", JOptionPane.ERROR_MESSAGE);
				else {
					TipoContactoDTO nuevoTipoDeContacto= new TipoContactoDTO(0, tipoContactoNuevo);
					this.agenda.agregarTipoContacto(nuevoTipoDeContacto);
					this.llenarTablaTipoContacto();
					this.llenarCombos();
				}
		}
		
		private void editarTipoContacto() {
			JTable tablaTipoContacto = this.ventanaTipoContacto.getTablaTipoContacto();
			int filaSeleccionada = tablaTipoContacto.getSelectedRow();
			if( filaSeleccionada == -1 ) {
				JOptionPane.showMessageDialog(this.ventanaTipoContacto, "No hay ningun tipo de contacto seleccionada.", "Error", JOptionPane.WARNING_MESSAGE);
			} else {
				TipoContactoDTO tipoContactoAEditar = (TipoContactoDTO) tablaTipoContacto.getValueAt(filaSeleccionada, 0);
				String nuevoNombre = JOptionPane.showInputDialog(ventanaTipoContacto, "Ingrese un nuevo nombre para el tipo de contacto: ", tipoContactoAEditar.getTipoContacto());
				if( nuevoNombre != null )
					if( nuevoNombre.trim().isEmpty() )
						JOptionPane.showMessageDialog(ventanaLocalidad, "El nuevo nombre del tipo de contacto no puede estar vacío.", "No se pudo editar el tipo de contacto", JOptionPane.ERROR_MESSAGE);
					else {
						tipoContactoAEditar.setTipoContacto(nuevoNombre);
						this.agenda.actualizarTipoContacto(tipoContactoAEditar);
						this.llenarTablaTipoContacto();
						this.llenarCombos();
						this.llenarTabla();
					}
			}
		}
		
		private void borrarTipoContacto() {			
			int[] filasSeleccionadas = this.ventanaTipoContacto.getTablaTipoContacto().getSelectedRows();
			if( filasSeleccionadas.length == 0 )
				JOptionPane.showMessageDialog(this.ventanaTipoContacto, "No hay ningun tipo de contacto seleccionado.", "Error", JOptionPane.WARNING_MESSAGE);
			else {
				TipoContactoDTO tipoContactoABorrar;
				for( int filaSeleccionada : filasSeleccionadas ) {
					tipoContactoABorrar = (TipoContactoDTO) this.ventanaTipoContacto.getTablaTipoContacto().getValueAt(filaSeleccionada, 0);
					if( !this.agenda.borrarTipoContacto(tipoContactoABorrar) )
						JOptionPane.showMessageDialog(ventanaTipoContacto, "El tipo de contacto " + tipoContactoABorrar.getTipoContacto() + " no se puede borrar porque está relacionada con uno o más contactos.\n Edite los contactos de este tipo antes de borrar.", "No se pudo borrar el tipo de contacto", JOptionPane.ERROR_MESSAGE);
					}
				this.llenarTablaTipoContacto();
				this.llenarCombos();
			}
		}
		
		
		// ** Personas **
		private void ventanaAgregarPersona() {
			this.ventanaPersona.mostrarVentana();
		}
		
		private void ventanaEditarContacto() {
			JTable tablaPersonas = this.vista.getTablaPersonas();
			int filaSeleccionada = tablaPersonas.getSelectedRow();
			if( filaSeleccionada == -1 ) {
				JOptionPane.showMessageDialog(this.vista.getFrame(), "No hay ningún contacto seleccionado.", "Error", JOptionPane.WARNING_MESSAGE);
			} else {
				this.ventanaEditarContacto                        .setIdContacto((Integer) tablaPersonas.getModel().getValueAt(filaSeleccionada, 0));
				this.ventanaEditarContacto.getTxtNombre()         .setText(String.valueOf(tablaPersonas.getModel().getValueAt(filaSeleccionada, 1)));
				this.ventanaEditarContacto.getTxtTelefono()       .setText(String.valueOf(tablaPersonas.getModel().getValueAt(filaSeleccionada, 2)));
				this.ventanaEditarContacto.getTxtCalle()          .setText(String.valueOf(tablaPersonas.getModel().getValueAt(filaSeleccionada, 3)));
				this.ventanaEditarContacto.getTxtAltura()         .setText(String.valueOf(tablaPersonas.getModel().getValueAt(filaSeleccionada, 4)));
				this.ventanaEditarContacto.getTxtPiso()           .setText(String.valueOf(tablaPersonas.getModel().getValueAt(filaSeleccionada, 5)));
				this.ventanaEditarContacto.getTxtDepto()          .setText(String.valueOf(tablaPersonas.getModel().getValueAt(filaSeleccionada, 6)));
				LocalidadDTO localidadSeleccionada                = (LocalidadDTO) tablaPersonas.getModel().getValueAt(filaSeleccionada, 7);
				this.ventanaEditarContacto.getComboLocalidad()    .setSelectedItem(localidadSeleccionada);
				this.ventanaEditarContacto.getTxtEmail()          .setText(String.valueOf(tablaPersonas.getModel().getValueAt(filaSeleccionada, 8)));
				LocalDate fechaDeNacimiento                       = (LocalDate) tablaPersonas.getModel().getValueAt(filaSeleccionada, 9);
				this.ventanaEditarContacto.getDatePiker()         .setDate(fechaDeNacimiento);
				this.ventanaEditarContacto.getComboTipoContacto() .setSelectedItem(tablaPersonas.getModel().getValueAt(filaSeleccionada, 10));
				this.ventanaEditarContacto                        .mostrarVentana();
			}
		}
		
		private void guardarPersona() {
			PersonaDTO nuevaPersona = new PersonaDTO(0,this.ventanaPersona.getTxtNombre().getText(),
					ventanaPersona.getTxtTelefono().getText(),
					ventanaPersona.getTxtCalle().getText(),
					ventanaPersona.getTxtAltura().getText(),
					ventanaPersona.getTxtPiso().getText(),
					ventanaPersona.getTxtDepto().getText(),
					(LocalidadDTO) ventanaPersona.getComboLocalidad().getSelectedItem(),
					ventanaPersona.getTxtEmail().getText(),
					ventanaPersona.getDatePicker().getDate(),
					(TipoContactoDTO) ventanaPersona.getComboTipoContacto().getSelectedItem());
			try {
				this.agenda.agregarPersona(nuevaPersona);
				this.llenarTabla();
				this.ventanaPersona.cerrar();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(ventanaPersona, e.getMessage(), "No se pudo guardar el contacto", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		public void editarContacto() {
			VentanaEditarContacto ventana = this.ventanaEditarContacto;
			PersonaDTO personaAEditar = new PersonaDTO(ventana.getIdContacto(),
													   ventana.getTxtNombre().getText(),
													   ventana.getTxtTelefono().getText(), 
													   ventana.getTxtCalle().getText(), 
													   ventana.getTxtAltura().getText(), 
													   ventana.getTxtPiso().getText(), 
													   ventana.getTxtDepto().getText(), 
													   (LocalidadDTO) ventana.getComboLocalidad().getSelectedItem(), 
													   ventana.getTxtEmail().getText(),
													   ventana.getDatePiker().getDate(),
													   (TipoContactoDTO) ventana.getComboTipoContacto().getSelectedItem());
			try {
				this.agenda.actualizarContacto(personaAEditar);
				this.llenarTabla();
				this.llenarCombos();
				this.ventanaEditarContacto.cerrar();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(ventanaEditarContacto, e.getMessage(), "No se pudo editar el contacto", JOptionPane.ERROR_MESSAGE);
			}
		}

		public void borrarPersona() {
			int[] filas_seleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			for (int fila:filas_seleccionadas) {
				this.agenda.borrarPersona(this.personas_en_tabla.get(fila));
			}
			this.llenarTabla();
		}
		
		
		//** Reporte **
		private void mostrarReporte() {
			List<PersonaDTO> personas = agenda.obtenerPersonasOrdenadas();
			if( personas == null || personas.size() == 0 )
				JOptionPane.showMessageDialog(this.vista.getFrame(), "No existen personas para hacer el reporte", "Reporte", JOptionPane.WARNING_MESSAGE);
			else {
				ReporteAgenda reporte = new ReporteAgenda(personas);
				reporte.mostrar();
			}
		}
		
		
		// ** LLenar **
		private void llenarCombos() {
			this.ventanaEditarContacto.getComboLocalidad().removeAllItems();
			this.ventanaEditarContacto.getComboTipoContacto().removeAllItems();
			this.ventanaPersona.getComboLocalidad().removeAllItems();
			this.ventanaPersona.getComboTipoContacto().removeAllItems();
			
			List<LocalidadDTO> localidades = this.agenda.obtenerLocalidades();
			List<TipoContactoDTO> tiposDeContacto = this.agenda.obtenerTipoContacto();
			for( LocalidadDTO localidad : localidades ) {
				this.ventanaEditarContacto.getComboLocalidad().addItem(localidad);
				this.ventanaPersona.getComboLocalidad().addItem(localidad);
			}
			for( TipoContactoDTO tipo : tiposDeContacto ) {
				this.ventanaEditarContacto.getComboTipoContacto().addItem(tipo);
				this.ventanaPersona.getComboTipoContacto().addItem(tipo);
			}
		}
		
		private void llenarTabla() {
			this.vista.getModelPersonas().setRowCount(0);
			this.vista.getModelPersonas().setColumnCount(0);
			this.vista.getModelPersonas().setColumnIdentifiers(this.vista.getNombreColumnas());
			
			this.personas_en_tabla = agenda.obtenerPersonas();
			for (int i = 0; i < this.personas_en_tabla.size(); i ++)
			{
				Object[] fila = {	this.personas_en_tabla.get(i).getIdPersona(),
									this.personas_en_tabla.get(i).getNombre(), 
									this.personas_en_tabla.get(i).getTelefono(),
									this.personas_en_tabla.get(i).getCalle(),
									this.personas_en_tabla.get(i).getAltura(),
									this.personas_en_tabla.get(i).getPiso(),
									this.personas_en_tabla.get(i).getDepto(),
									this.personas_en_tabla.get(i).getLocalidad(),
									this.personas_en_tabla.get(i).getEmail(),
									this.personas_en_tabla.get(i).getCumple(),
									this.personas_en_tabla.get(i).getTipoContacto()
						        };
				this.vista.getModelPersonas().addRow(fila);
			}
			TableColumnModel tcm = this.vista.getTablaPersonas().getColumnModel();
			tcm.removeColumn(tcm.getColumn(0));
		}
		
		private void llenarTablaLocalidades() {
			this.ventanaLocalidad.getModelLocalidades().setRowCount(0);
			this.ventanaLocalidad.getModelLocalidades().setColumnCount(0);
			this.ventanaLocalidad.getModelLocalidades().setColumnIdentifiers(this.ventanaLocalidad.getNombreColumnas());
			for( LocalidadDTO localidad : agenda.obtenerLocalidades() ) {
				Object[] fila = {localidad};
				this.ventanaLocalidad.getModelLocalidades().addRow(fila);
			}
		}
		
		private void llenarTablaTipoContacto() {
			this.ventanaTipoContacto.getModelTipoContacto().setRowCount(0);
			this.ventanaTipoContacto.getModelTipoContacto().setColumnCount(0);
			this.ventanaTipoContacto.getModelTipoContacto().setColumnIdentifiers(this.ventanaTipoContacto.getNombreColumnas());
			for( TipoContactoDTO tipoContacto : agenda.obtenerTipoContacto() ) {
				Object[] fila = {tipoContacto};
				this.ventanaTipoContacto.getModelTipoContacto().addRow(fila);
			}
		}
}