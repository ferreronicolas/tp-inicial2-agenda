package dto;

public class TipoContactoDTO {
	
	private int idTipoContacto;
	private String tipoContacto;
	
	public TipoContactoDTO(int idTipoContacto, String tipoContacto) {
		this.idTipoContacto = idTipoContacto;
		this.tipoContacto = tipoContacto;
	}

	public int getIdTipoContacto() {
		return idTipoContacto;
	}

	public void setIdTipoContacto(int idTipoContacto) {
		this.idTipoContacto = idTipoContacto;
	}

	public String getTipoContacto() {
		return tipoContacto;
	}

	public void setTipoContacto(String tipoContacto) {
		this.tipoContacto = tipoContacto;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idTipoContacto;
		result = prime * result + ((tipoContacto == null) ? 0 : tipoContacto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoContactoDTO other = (TipoContactoDTO) obj;
		if (idTipoContacto != other.idTipoContacto)
			return false;
		if (tipoContacto == null) {
			if (other.tipoContacto != null)
				return false;
		} else if (!tipoContacto.equals(other.tipoContacto))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return this.tipoContacto;
	}
}
