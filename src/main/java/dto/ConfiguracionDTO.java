package dto;

public class ConfiguracionDTO {

	private String puerto;
	private String nombre;
	private String usuario;
	private String contrasena;
	
	public ConfiguracionDTO(String puerto, String nombre, String usuario, String contrasena) {
		this.puerto     = puerto;
		this.nombre     = nombre;
		this.usuario    = usuario;
		this.contrasena = contrasena;
	}

	public String getPuerto() {
		return puerto;
	}

	public String getNombre() {
		return nombre;
	}

	public String getUsuario() {
		return usuario;
	}

	public String getContrasena() {
		return contrasena;
	}
}
