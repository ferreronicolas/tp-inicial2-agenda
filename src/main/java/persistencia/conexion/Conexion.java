package persistencia.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.log4j.Logger;

import dto.ConfiguracionDTO;

public class Conexion 
{
	public static Conexion instancia;
	private Connection connection;
	private static Logger LOG = Logger.getLogger(Conexion.class);	
	
	private Conexion(ConfiguracionDTO configuracion) throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		this.connection = DriverManager.getConnection("jdbc:mysql://localhost:" + configuracion.getPuerto() + "/" + configuracion.getNombre(), configuracion.getUsuario(), configuracion.getContrasena());
		LOG.info("Conexión exitosa");
	}
	
	
	public static Conexion getConexion(ConfiguracionDTO configuracion) {								
		try {
			instancia = new Conexion(configuracion);
		} catch (Exception e) {
			LOG.error("Conexión fallida", e);
			System.out.println("Excepcion: " + e.getMessage());
			instancia = null;
		}
		return instancia;
	}
	
	public static Conexion getConexion() {
		if( instancia == null )
			return null;
		else
			return instancia;
	}

	public Connection getSQLConexion() 
	{
		return this.connection;
	}
	
	public void cerrarConexion()
	{
		try 
		{
			this.connection.close();
			LOG.info("Conexion cerrada");
		}
		catch (SQLException e) 
		{
			LOG.error("Error al cerrar la conexión!", e);
		}
		instancia = null;
	}
}
