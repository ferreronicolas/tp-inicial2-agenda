package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dto.TipoContactoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoContactoDAO;

public class TipoContactoDAOSQL implements TipoContactoDAO{

	private static final String insert  = "INSERT INTO tipoContacto(idTipoContacto, tipoContacto) VALUES(?, ?)";
	private static final String delete  = "DELETE FROM tipoContacto WHERE idTipoContacto= ?";
	private static final String update  = "UPDATE tipoContacto SET tipoContacto = ? WHERE idTipoContacto = ?";
	private static final String readall = "SELECT * FROM tipoContacto";
	private static final String select  = "SELECT * FROM tipoContacto WHERE idTipoContacto = ?";
	
	private Logger log = Logger.getLogger(TipoContactoDTO.class);

	@Override
	public boolean insert(TipoContactoDTO tipoContacto) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		
		try {
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setInt(1, tipoContacto.getIdTipoContacto());
			statement.setString(2, tipoContacto.getTipoContacto());
			if( statement.executeUpdate() > 0 )
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean delete(TipoContactoDTO tipoContacto) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setInt(1, tipoContacto.getIdTipoContacto());
			if( statement.executeUpdate() > 0 )
				return true;
		} catch (SQLException e) {
			log.info(e.getMessage());
			//e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public boolean update(TipoContactoDTO tipoContacto) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, tipoContacto.getTipoContacto());
			statement.setInt(2, tipoContacto.getIdTipoContacto());
			if( statement.executeUpdate() > 0 )
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public List<TipoContactoDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<TipoContactoDTO> tipoContactoList = new ArrayList<TipoContactoDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			
			while( resultSet.next() ) {
				
				tipoContactoList.add(new TipoContactoDTO(resultSet.getInt("idTipoContacto"), resultSet.getString("tipoContacto")));
			}
		} 
		catch (SQLException e) {
			
			e.printStackTrace();
		}
		return tipoContactoList;
	}

	@Override
	public TipoContactoDTO select(int idTipoContacto) {
		TipoContactoDTO tipoContacto = null;
		ResultSet resultSet;
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		
		try {
			statement = conexion.getSQLConexion().prepareStatement(select);
			statement.setInt(1, idTipoContacto);
			resultSet = statement.executeQuery();
			
			while (resultSet.next()) {
				tipoContacto = new TipoContactoDTO(resultSet.getInt("idTipoContacto"), resultSet.getString("tipoContacto"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return tipoContacto;
	}
}
