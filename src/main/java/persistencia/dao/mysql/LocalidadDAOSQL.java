package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dto.LocalidadDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.LocalidadDAO;

public class LocalidadDAOSQL implements LocalidadDAO {
	
	private static final String insert  = "INSERT INTO localidades(idLocalidad, nombre) VALUES(?, ?)";
	private static final String delete  = "DELETE FROM localidades WHERE idLocalidad= ?";
	private static final String update  = "UPDATE localidades SET nombre = ? WHERE idLocalidad = ?";
	private static final String readall = "SELECT * FROM localidades";
	private static final String select  = "SELECT * FROM localidades WHERE idLocalidad = ?";
	
	private Logger log = Logger.getLogger(LocalidadDAOSQL.class);

	@Override
	public boolean insert(LocalidadDTO localidad) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		
		try {
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setInt(1, localidad.getIdLocalidad());
			statement.setString(2, localidad.getNombre());
			if( statement.executeUpdate() > 0 )
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(LocalidadDTO localidad) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setInt(1, localidad.getIdLocalidad());
			if( statement.executeUpdate() > 0 )
				return true;
		} catch (SQLException e) {
			log.info(e.getMessage());
		}
		return false;
	}

	@Override
	public boolean update(LocalidadDTO localidad) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, localidad.getNombre());
			statement.setInt(2, localidad.getIdLocalidad());
			if( statement.executeUpdate() > 0 )
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<LocalidadDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<LocalidadDTO> localidades = new ArrayList<LocalidadDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while( resultSet.next() ) {
				LocalidadDTO localidad = new LocalidadDTO(resultSet.getInt("idLocalidad"), resultSet.getString("nombre"));
				localidades.add(localidad);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return localidades;
	}

	@Override
	public LocalidadDTO select(int idLocalidad) {
		LocalidadDTO localidad = null;
		ResultSet resultSet;
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		
		try {
			statement = conexion.getSQLConexion().prepareStatement(select);
			statement.setInt(1, idLocalidad);
			resultSet = statement.executeQuery();
			resultSet.next();
			localidad = new LocalidadDTO(resultSet.getInt("idLocalidad"), resultSet.getString("nombre"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return localidad;
	}
}
