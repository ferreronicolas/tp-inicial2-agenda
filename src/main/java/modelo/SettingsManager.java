package modelo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Properties;

import org.apache.log4j.Logger;

import dto.ConfiguracionDTO;

public class SettingsManager {
	
	private static final String FILE_NAME     = "agenda.properties";
	private static final String PATH          = System.getProperty("user.home") + System.getProperty("file.separator") + FILE_NAME;
	private static final String PORT_KEY_NAME = "db.port";
	private static final String DB_KEY_NAME   = "db.name";
	private static final String USER_KEY_NAME = "db.user";
	private static final String PASS_KEY_NAME = "db.pass";
	private static final Logger LOG           = Logger.getLogger(SettingsManager.class);
	
	private Properties settings;
	
	public SettingsManager() {
		settings = new Properties();
		if( existeArchivo() )
			loadSettings();
	}
	
	private boolean existeArchivo() {
		return Files.exists(Paths.get(PATH), LinkOption.NOFOLLOW_LINKS);
	}
	
	public boolean existeSettings() {
		return existeArchivo() && settings != null && validarSettings(settings);
	}
	
	private static boolean validarSettings(Properties settings) {
		if( settings == null )
			return false;
		if( !(settings.containsKey(PORT_KEY_NAME) 
				&& settings.containsKey(DB_KEY_NAME) 
				&& settings.containsKey(USER_KEY_NAME) 
				&& settings.containsKey(PASS_KEY_NAME)) )
			return false;
		return !settings.getProperty(DB_KEY_NAME).trim().isEmpty()
				&& !settings.getProperty(PASS_KEY_NAME).trim().isEmpty()
				&& !settings.getProperty(USER_KEY_NAME).trim().isEmpty()
				&& !settings.getProperty(PORT_KEY_NAME).trim().isEmpty();
	}
	
	private boolean loadSettings() {
		try {
			FileInputStream in = new FileInputStream(PATH);
			settings.load(in);
			in.close();
			return true;
		} catch (FileNotFoundException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
	}
	
	public boolean setSettings(ConfiguracionDTO configuracion) {
		if( configuracion == null )
			return false;
		try {
			FileOutputStream out = new FileOutputStream(PATH);
			settings = new Properties();
			settings.setProperty(DB_KEY_NAME, configuracion.getNombre());
			settings.setProperty(PASS_KEY_NAME, configuracion.getContrasena());
			settings.setProperty(PORT_KEY_NAME, configuracion.getPuerto());
			settings.setProperty(USER_KEY_NAME, configuracion.getUsuario());
			settings.store(out, "-- DataBase Settings --");
			out.close();
			return true;
		} catch (FileNotFoundException e) {
			LOG.error(e.getMessage());
			return false;
		} catch (IOException e) {
			LOG.error(e.getMessage());
			return false;
		}
	}
	
	public ConfiguracionDTO getSettings() {
		if( !validarSettings(settings) )
			return null;
		return new ConfiguracionDTO(settings.getProperty(PORT_KEY_NAME),
									settings.getProperty(DB_KEY_NAME),
									settings.getProperty(USER_KEY_NAME),
									settings.getProperty(PASS_KEY_NAME));
	}
	
	public static Locale getLocale() {
		return new Locale("es", "ARG");
	}
}
