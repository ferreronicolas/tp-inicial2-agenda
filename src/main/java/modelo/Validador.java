package modelo;

import dto.ConfiguracionDTO;
import dto.PersonaDTO;
import persistencia.conexion.Conexion;

public class Validador {
	
	public static boolean validadPersona(PersonaDTO persona) {
		if( persona.getNombre() == null || persona.getTelefono() == null || persona.getCumple() == null )
			return false;
		if( persona.getNombre().trim().isEmpty() || persona.getTelefono().trim().isEmpty() )
			return false;
		return true;
	}
	
	public static boolean validarConfiguracion(ConfiguracionDTO conf) {
		if( conf == null )
			return false;
		if( conf.getPuerto() == null || conf.getNombre() == null || conf.getContrasena() == null || conf.getUsuario() == null )
			return false;
		if( conf.getPuerto().trim().isEmpty() || conf.getNombre().trim().isEmpty() || conf.getContrasena().trim().isEmpty() || conf.getUsuario().trim().isEmpty() )
			return false;
		return true;
	}
	
	public static boolean validarConexion(ConfiguracionDTO conf) {
		if( !validarConfiguracion(conf) )
			return false;
		Conexion conexion = Conexion.getConexion(conf);
		if( conexion == null )
			return false;
		return true;
	}
}
