package modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.TipoContactoDAO;


public class Agenda 
{
	private PersonaDAO persona;	
	private LocalidadDAO localidad;
	private TipoContactoDAO tipoContacto;
	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
		this.tipoContacto = metodo_persistencia.createTipoContactoDAO();
	}
	
	public void agregarPersona(PersonaDTO nuevaPersona) throws Exception {
		if( !Validador.validadPersona(nuevaPersona) )
			throw new Exception("Los campos nombre, teléfono y cumpleaños no pueden estar vacíos.");
		this.persona.insert(nuevaPersona);
	}
	
	public void actualizarContacto(PersonaDTO personaAEditar) throws Exception {
		if( !Validador.validadPersona(personaAEditar) )
			throw new Exception("Los campos nombre, teléfono y compleaños no pueden estar vacíos.");
		this.persona.update(personaAEditar);
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar) {
		this.persona.delete(persona_a_eliminar);
	}
	
	public List<PersonaDTO> obtenerPersonas() {
		List<PersonaDTO> personasSinLocalidadNiTipo = this.persona.readAll();
		return personasSinLocalidadNiTipo.stream()
								   .map(p -> {
									   			p.setLocalidad(this.localidad.select(p.getLocalidad().getIdLocalidad()));
									   			p.setTipoContacto(this.tipoContacto.select(p.getTipoContacto().getIdTipoContacto()));
												return p;
											 })
								   .collect(Collectors.toList());	
	}
	
	public List<PersonaDTO> obtenerPersonasOrdenadas()
	{
		List<PersonaDTO> personas = this.persona.readAll();
		personas.forEach(f -> f.setLocalidad(this.getLocalidad(f.getLocalidad().getIdLocalidad())));
		
		Map<Integer, List<PersonaDTO>> personasPorMes = personas.stream().collect(Collectors.groupingBy(PersonaDTO::getMesCumpleInt));
		personasPorMes.forEach((k,v)->Collections.sort(v, (x, y) -> x.getLocalidad().getNombre().compareTo(y.getLocalidad().getNombre())));		
		personas = new ArrayList<PersonaDTO>();
		for (List<PersonaDTO> listPersona : personasPorMes.values()) {
			personas.addAll(listPersona);
		}
		return personas;		
	}
	
	public void agregarLocalidad(LocalidadDTO nuevaLocalidad) {
		this.localidad.insert(nuevaLocalidad);
	}
	
	public boolean borrarLocalidad(LocalidadDTO localidadABorrar) {
		return this.localidad.delete(localidadABorrar);
	}
	
	public void actualizarLocalidad(LocalidadDTO localidadAActualizar) {
		this.localidad.update(localidadAActualizar);
	}
	
	public List<LocalidadDTO> obtenerLocalidades() {
		return this.localidad.readAll();
	}
	
	public void agregarTipoContacto(TipoContactoDTO nuevoTipoContacto) {
		this.tipoContacto.insert(nuevoTipoContacto);
	}
	
	public boolean borrarTipoContacto(TipoContactoDTO tipoContactoABorrar) {
		return this.tipoContacto.delete(tipoContactoABorrar);
	}
	
	public void actualizarTipoContacto(TipoContactoDTO tipoContactoAActualizar) {
		this.tipoContacto.update(tipoContactoAActualizar);
	}
	
	public List<TipoContactoDTO> obtenerTipoContacto() {
		return this.tipoContacto.readAll();
	}
	
	public LocalidadDTO getLocalidad (int idLocalidad) {
		return this.localidad.select(idLocalidad);
	}
	
	public TipoContactoDTO getTipoContacto (int idTipoContacto) {
		return this.tipoContacto.select(idTipoContacto);
	}
	
	public Locale getLocale() {
		return SettingsManager.getLocale();
	}
	
}
