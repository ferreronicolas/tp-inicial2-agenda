package main;

import org.apache.log4j.Logger;

import modelo.Agenda;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.Controlador;
import presentacion.vista.Vista;

public class Main {
	
	private static Logger LOG = Logger.getLogger(Conexion.class);	
	
	public static void main(String[] args) {
		Vista vista = new Vista();
		Controlador controlador = new Controlador(vista);
		try {
		DAOAbstractFactory factory = new DAOSQLFactory(controlador.configurarConexion());
		Agenda modelo = new Agenda( factory );
		controlador = new Controlador(vista, modelo);
		controlador.inicializar();		
		}catch(Exception e){
			LOG.error("Configure la conexion.", e);
			System.out.println("Excepcion: " + e.getMessage());
			vista.getFrame().dispose();
		}
	}
}
