CREATE DATABASE `agenda`;
USE agenda;
CREATE TABLE localidades (
	idLocalidad int(11) NOT NULL AUTO_INCREMENT,
	nombre varchar(45) NOT NULL,
	PRIMARY KEY (idLocalidad)
);
CREATE TABLE tipoContacto (
	idTipoContacto int(11) NOT NULL AUTO_INCREMENT,
	tipoContacto varchar(20) NOT NULL,
	PRIMARY KEY(idTipoContacto)
);
CREATE TABLE `personas`
(
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Telefono` varchar(20) NOT NULL,
  calle varchar(20),
  altura varchar(6),
  piso varchar(6),
  depto varchar(6),
  localidad int(11),
  email varchar(20),
  cumple date,
  tipoContacto int(11),
  PRIMARY KEY (`idPersona`),
  FOREIGN KEY(localidad) REFERENCES localidades(idLocalidad),
  FOREIGN KEY(tipoContacto) REFERENCES tipoContacto(idTipoContacto)
);
